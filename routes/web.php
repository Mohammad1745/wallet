<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/wallet', 'WalletController@index')->name('wallet.index');
Route::get('/wallet/{wallet}', 'WalletController@show')->name('wallet.show');
Route::post('/wallet/store', 'WalletController@store')->name('wallet.store');
