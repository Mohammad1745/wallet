@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-9 h4 font-weight-bold">{{$wallet->title}}</div>
                            <div class="col-3 "><!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                                    Send Money
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <h4 class="text-danger">{{$errors->first('title')}}</h4>
                        @if(isset($wallet))
                            <div class="col-12 row">
                                <h4>{{$wallet->balance}} tk</h4>
                            </div>
                        @else
                            <h4>No Wallet Found</h4>
                        @endif
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Create New Wallet</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{url('api/transfer-balance')}}" method="post">
                                        @csrf

                                        <input type="hidden" name="fromUser" class="form-control" id="from-user" value="{{$wallet->id}}">
                                        <div class="form-group">
                                            <label for="title">Receiver Wallet Title</label>
                                            <select name="toUser" class="form-control" id="to-user" size="10">
                                                @if(sizeof($wallets) == 0)
                                                    <option value="" disabled>No Wallet Available!</option>
                                                @else
                                                    @foreach($wallets as $wallet)
                                                        <option value="{{$wallet->id}}">{{$wallet->title}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Transfer Amount</label>
                                            <input type="text" name="transferAmount" class="form-control" id="transfer-amount" value="{{old('transferAmount')}}">
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" type="button" class="btn btn-primary">Confirm</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
