@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-9 h4 font-weight-bold">Wallet</div>
                            <div class="col-3 "><!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                                    Create Wallet
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <h4 class="text-danger">{{$errors->first('title')}}</h4>
                        @if(isset($wallets[0]))
                            <div class="col-12 row">
                                @foreach($wallets as $wallet)
                                    <h4 class="col-6">
                                        <a class="link-stabilize" href="{{route('wallet.show', ['wallet' => $wallet->id])}}">{{$wallet->title}}</a>
                                    </h4>
                                    <h4 class="col-6">{{$wallet->balance}} TK</h4>
                                @endforeach
                            </div>
                        @else
                            <h4>No Wallet Found</h4>
                        @endif
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Create New Wallet</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('wallet.store')}}" method="post">
                                        @csrf

                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" name="title" class="form-control" id="title" value="{{old('title')}}">
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" type="button" class="btn btn-primary">Create</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
