@component('mail::message')
#The Transaction is completed

You have successfully transferred: <br>
<strong>
    Balance: {{$data['transferAmount']}} <br>
    From   : {{$data['senderWalletTitle']}} <br>
    To     : {{$data['receiverWalletTitle']}} <br>
</strong>
@component('mail::button', ['url' => 'http://127.0.0.1:8000/wallet'])
Check Wallets
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
