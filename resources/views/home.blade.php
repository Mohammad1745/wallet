@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    @if(!$wallet)
                        <div>
                            <h3><a class='link-stabilize' href="#">Create Wallet First</a></h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
