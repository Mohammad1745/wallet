<?php

namespace App\Jobs;

use App\Services\WalletService;
use App\Wallet;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class BalanceTransferJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $senderWalletId;
    protected $receiverWalletId;
    protected $transferAmount;

    /**
     * Create a new job instance.
     *
     * @param $senderWalletId
     * @param $receiverWalletId
     * @param $transferAmount
     */
    public function __construct($senderWalletId, $receiverWalletId, $transferAmount)
    {
        $this->senderWalletId = $senderWalletId;
        $this->receiverWalletId = $receiverWalletId;
        $this->transferAmount = $transferAmount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        try {
//            DB::beginTransaction();
            //decrease sender balance
            Wallet::where('id', $this->senderWalletId)->decrement('balance', $this->transferAmount);
            //increase receiver balance
            Wallet::where('id', $this->receiverWalletId)->increment('balance', $this->transferAmount);

//            DB::commit();
//
//        }catch (\Exception $e){
//
//            DB::rollBack();
//        }
    }
}
