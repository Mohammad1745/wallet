<?php

namespace App\Jobs;

use App\Mail\SendConfirmationToReceiverMail;
use App\Mail\SendConfirmationToSenderMail;
use App\Wallet;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendConfirmationToReceiverJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $senderWalletId;
    protected $receiverWalletId;
    protected $transferAmount;

    /**
     * Create a new job instance.
     *
     * @param $senderWalletId
     * @param $receiverWalletId
     * @param $transferAmount
     */
    public function __construct($senderWalletId, $receiverWalletId, $transferAmount)
    {
        $this->senderWalletId = $senderWalletId;
        $this->receiverWalletId = $receiverWalletId;
        $this->transferAmount = $transferAmount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data['senderWalletTitle'] = Wallet::find($this->senderWalletId)->title;
        $data['receiverWalletTitle'] = Wallet::find($this->receiverWalletId)->title;
        $data['transferAmount'] = $this->transferAmount;
        $data['receiverWalletId'] = $this->receiverWalletId;

        Mail::to(Wallet::find($this->receiverWalletId)->user->email)->send(new SendConfirmationToReceiverMail($data));
    }
}
