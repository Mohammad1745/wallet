<?php


namespace App\Services;


use App\Exceptions\CantTransferToSameWalletException;
use App\Exceptions\ReceiverWalletNotFoundException;
use App\Exceptions\SenderWalletNotFoundException;
use App\Exceptions\WalletDoesNotHaveEnoughBalanceException;
use App\Jobs\BalanceTransferJob;
use App\Jobs\SendConfirmationToReceiverJob;
use App\Jobs\SendConfirmationToSenderJob;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WalletService
{
    public function createNewWallet($request)
    {
        $wallets = Auth::user()->wallets;
        $balance = sizeof($wallets)>0 ? 0 : 100;
        return Wallet::create([
            'title' => $request->title,
            'user_id' => Auth::user()->id,
            'balance' => $balance,
        ]);
    }

    public function sendConfirmationMailToSenderAndReceiver(int $senderWalletId, int $receiverWalletId, float $transferAmount)
    {
        dispatch(new SendConfirmationToSenderJob($senderWalletId, $receiverWalletId, $transferAmount))
            ->onQueue('send-confirmation-to-sender')
            ->delay(Carbon::now()->addSeconds(5));

        if(Wallet::find($senderWalletId)->user_id != Wallet::find($receiverWalletId)->user_id ){
            dispatch(new SendConfirmationToReceiverJob($senderWalletId, $receiverWalletId, $transferAmount))
            ->onQueue('send-confirmation-to-receiver')
            ->delay(Carbon::now()->addSeconds(5));
        }
    }

    public function transfer(int $senderWalletId, int $receiverWalletId, float $transferAmount)
    {
        try {
            //check if the user is trying to send balance in his own wallet,
            if($senderWalletId == $receiverWalletId){
                throw new CantTransferToSameWalletException('Same wallet transaction is restricted!');
            }
            //check if the sender wallet exists or not
            $senderWallet = Wallet::find($senderWalletId);
            //dd($senderWallet);
            if(!$senderWallet){
                throw new SenderWalletNotFoundException('Sender Wallet not found!');
            }
            //check if the sender wallet has enough balance
            $senderHasEnoughBalance = $this->hasEnoughBalanceToTransfer($senderWalletId, $transferAmount);
            if (!$senderHasEnoughBalance){
                throw new WalletDoesNotHaveEnoughBalanceException('Not Enough Balance!');
            }
            $receiverWallet = Wallet::find($receiverWalletId);
            if(!$receiverWallet){
                throw new ReceiverWalletNotFoundException('Receiver Wallet not found');
            }

            dispatch(new BalanceTransferJob($senderWalletId, $receiverWalletId, $transferAmount))
                ->onQueue('transfer-balance')
                ->delay(Carbon::now()->addSeconds(5));

            return [
                'success' => true,
                'message' => 'Balance transfer request is submitted, Please wait for your transaction.'
            ];
        }catch (\Exception $e){
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    public function hasEnoughBalanceToTransfer(int $walletId, float $transferAmount)
    {
        $senderWalletBalance = Wallet::where('id', $walletId)->first()->balance;
        if ($senderWalletBalance >= $transferAmount){
            return true;
        }else{
            return false;
        }
    }
}
