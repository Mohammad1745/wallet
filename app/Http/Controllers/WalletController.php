<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWalletRequest;
use App\Jobs\BalanceTransferJob;
use App\Services\WalletService;
use App\Wallet;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use test\Mockery\Adapter\Phpunit\BaseClassStub;

class WalletController extends Controller
{
    private $walletService;

    public function __construct()
    {
        $this->walletService = new WalletService();
    }

    public function index()
    {
        $wallets = Wallet::where('user_id', Auth::user()->id)->get();

        return view('wallet.index', compact('wallets'));
    }

    public function show(Wallet $wallet)
    {
        if(isset($wallet)){
            $wallets = Wallet::where('id', '!=', $wallet->id)->get();
            return view('wallet.show', compact('wallet', 'wallets'));
        }
        else{
            return back()->with('error', 'Invalid Request!');
        }
    }

    public function store(CreateWalletRequest $request)
    {
        try {
            $transferResponse = $this->walletService->createNewWallet($request);

            return redirect(route('wallet.index'))->with('success', 'Wallet has been created successfully!');
        }catch (\Exception $e){
            return redirect(route('wallet.index'))->with('error', 'Wallet has not been created!');
        }
    }

    public function transferBalance(Request $request)
    {
        try {
            $transferResponse = $this->walletService->transfer(
                $request->fromUser,
                $request->toUser,
                $request->transferAmount
            );
            if($transferResponse['success'] == true){
                $this->walletService->sendConfirmationMailToSenderAndReceiver(
                    $request->fromUser,
                    $request->toUser,
                    $request->transferAmount
                );
            }
            return response()->json(['success' => true, 'message' => $transferResponse['message']]);
        }catch (\Exception $e){
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}
